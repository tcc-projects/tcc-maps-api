package gov.bomdestino.maps.api;

import gov.bomdestino.maps.pontogeografico.PontoGeograficoService;
import gov.bomdestino.maps.pontogeografico.data.PontoGeograficoDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@AllArgsConstructor
@RequestMapping("/mapa/pontos")
@RestController
@CrossOrigin(origins = "*")
public class PontoController {

    private final PontoGeograficoService pontoService;

    @PostMapping(consumes= MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Criação de um ponto simples no mapa",
            authorizations = {@Authorization(value = "acess_token")})
    public void criaPonto(@RequestBody @Valid PontoGeograficoDTO ponto){
        pontoService.salvar(ponto);
    }

}