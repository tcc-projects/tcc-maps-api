package gov.bomdestino.maps.api;

import gov.bomdestino.maps.etiquetas.EtiquetaService;
import gov.bomdestino.maps.etiquetas.data.EtiquetaDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RequestMapping(value="/mapa/etiquetas", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class EtiquetaController {

    private final EtiquetaService etiquetaService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Busca etiquetas disponíveis")
    public List<EtiquetaDTO> buscaEtiquetas(){
        return etiquetaService.buscaTodas();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Criação de etiquestas que idenfiquem as marcações no mapa",
            authorizations = {@Authorization(value = "acess_token")})
    public void criarEtiqueta(@Valid @RequestBody EtiquetaDTO etiquetaDTO){
        etiquetaService.salvar(etiquetaDTO);
    }
}
