package gov.bomdestino.maps.api;

import gov.bomdestino.maps.ocorrencias.OcorrenciaService;
import gov.bomdestino.maps.ocorrencias.data.OcorrenciaDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequestMapping(value= "mapa/ocorrencias")
@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class OcorrenciaController {

    private final OcorrenciaService ocorrenciaService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Criação de uma mancha a partir de um ponto no mapa",
            authorizations = {@Authorization(value = "acess_token")})
    public void criaOcorrencia(@RequestBody @Valid OcorrenciaDTO ocorrenciaDTO){
        ocorrenciaService.salvarOcorrencia(ocorrenciaDTO);
    }
}
