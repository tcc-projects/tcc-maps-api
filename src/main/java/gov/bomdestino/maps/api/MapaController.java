package gov.bomdestino.maps.api;

import gov.bomdestino.maps.etiquetas.data.EtiquetaDTO;
import gov.bomdestino.maps.commons.dto.MapaDeReferenciasDTO;
import gov.bomdestino.maps.referencias.MapaFacade;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value="/mapa", produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class MapaController {

    private final MapaFacade mapaFacade;

    @GetMapping("referencias")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Busca todas as informações georeferenciais cadastradas")
    public MapaDeReferenciasDTO buscaInformacoesGeoreferenciadas(){
        return mapaFacade.buscaReferencias();
    }
}
