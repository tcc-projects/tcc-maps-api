package gov.bomdestino.maps.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class OcorrenciaErroDTO {

	private String referencia;
	private String message;

	public OcorrenciaErroDTO(String message, String referencia) {
		this.message = message;
		this.referencia = referencia;
	}

}
