package gov.bomdestino.maps.api.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class ErroDTO {
   private LocalDateTime dataErro;
   private int httpCode;
   private List<OcorrenciaErroDTO> erros;

   public ErroDTO(){
       this.erros = new ArrayList<>();
   }

   public void addOcorrencia(OcorrenciaErroDTO ocorrencia){
       this.erros.add(ocorrencia);
   }
}
