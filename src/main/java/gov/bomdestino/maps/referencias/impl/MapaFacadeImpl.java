package gov.bomdestino.maps.referencias.impl;

import gov.bomdestino.maps.etiquetas.EtiquetaService;
import gov.bomdestino.maps.etiquetas.data.EtiquetaDTO;
import gov.bomdestino.maps.commons.dto.MapaDeReferenciasDTO;
import gov.bomdestino.maps.ocorrencias.OcorrenciaService;
import gov.bomdestino.maps.pontogeografico.PontoGeograficoService;
import gov.bomdestino.maps.referencias.MapaFacade;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@AllArgsConstructor
public class MapaFacadeImpl implements MapaFacade {
    private final PontoGeograficoService pontoService;
    private final OcorrenciaService ocorrenciaService;


    public MapaDeReferenciasDTO buscaReferencias() {
        MapaDeReferenciasDTO referencias = new MapaDeReferenciasDTO();
        pontoService.buscarTodos()
                    .forEach(referencias::addPonto);

        ocorrenciaService.buscaTodas()
                         .forEach(referencias::addOcorrencia);

        return referencias;
    }


}
