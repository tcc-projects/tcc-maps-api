package gov.bomdestino.maps.referencias;

import gov.bomdestino.maps.etiquetas.data.EtiquetaDTO;
import gov.bomdestino.maps.commons.dto.MapaDeReferenciasDTO;

import java.util.List;

public interface MapaFacade {
    MapaDeReferenciasDTO buscaReferencias();
}
