package gov.bomdestino.maps.ocorrencias.impl;

import gov.bomdestino.maps.ocorrencias.OcorrenciaService;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

interface OcorrenciaRepository extends JpaRepository<OcorrenciaModel, Long> {
    List<OcorrenciaModel> findAllByDataOcorrenciaBetween(LocalDate inicio, LocalDate fim);
    Optional<OcorrenciaModel> findByLatAndLngAndEtiqueta(BigDecimal lat, BigDecimal lng, String Etiqueta);
}
