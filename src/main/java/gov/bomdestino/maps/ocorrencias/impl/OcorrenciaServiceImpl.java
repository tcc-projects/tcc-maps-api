package gov.bomdestino.maps.ocorrencias.impl;

import gov.bomdestino.maps.etiquetas.EtiquetaService;
import gov.bomdestino.maps.ocorrencias.OcorrenciaService;
import gov.bomdestino.maps.ocorrencias.data.OcorrenciaDTO;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
class OcorrenciaServiceImpl implements OcorrenciaService {
    private final ModelMapper mapper;
    private final OcorrenciaRepository ocorrenciaRepository;
    private final EtiquetaService etiquetaService;

    @Override
    public void salvarOcorrencia(OcorrenciaDTO dto) {
        etiquetaService.validaEtiqueta(dto.getEtiqueta());
        ocorrenciaRepository.findByLatAndLngAndEtiqueta(dto.getLat(), dto.getLng(), dto.getEtiqueta())
                            .ifPresentOrElse(ocorrencia-> atualizaOcorrencia(dto, ocorrencia),
                                             () -> gravaOcorrencia(dto));
    }

    @Override
    public List<OcorrenciaDTO> buscaTodas() {
        return ocorrenciaRepository.findAll()
                                    .stream()
                                    .map(o-> mapper.map(o, OcorrenciaDTO.class))
                                    .collect(Collectors.toList());
    }

    private void gravaOcorrencia(OcorrenciaDTO ocorrenciaDTO){
        OcorrenciaModel model = mapper.map(ocorrenciaDTO, OcorrenciaModel.class);
        model.setDataOcorrencia(LocalDate.now());
        ocorrenciaRepository.save(model);
    }

    private void atualizaOcorrencia(OcorrenciaDTO ocorrenciaDTO, OcorrenciaModel model){
        model.addNumeroOcorrencia(ocorrenciaDTO.getNumeroOcorrencias());
        model.setDataOcorrencia(LocalDate.now());
        ocorrenciaRepository.save(model);
    }
}
