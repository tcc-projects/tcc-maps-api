package gov.bomdestino.maps.ocorrencias.impl;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name="ocorrencia")
@Getter
@Setter
class OcorrenciaModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(precision = 19, scale = 6, columnDefinition="DECIMAL(19,6)")
    private BigDecimal lat;
    @Column(precision = 19, scale = 6, columnDefinition="DECIMAL(19,6)")
    private BigDecimal lng;
    private Long numeroOcorrencias;
    private LocalDate dataOcorrencia;
    private String etiqueta;

    public void addNumeroOcorrencia(Long numeroOcorrencias){
        this.numeroOcorrencias = this.numeroOcorrencias + numeroOcorrencias;
    }
}
