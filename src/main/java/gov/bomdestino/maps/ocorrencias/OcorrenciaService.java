package gov.bomdestino.maps.ocorrencias;

import gov.bomdestino.maps.etiquetas.data.EtiquetaDTO;
import gov.bomdestino.maps.ocorrencias.data.OcorrenciaDTO;

import java.time.LocalDate;
import java.util.List;

public interface OcorrenciaService {
    void salvarOcorrencia(OcorrenciaDTO ocorrencia);
    List<OcorrenciaDTO> buscaTodas();
}
