package gov.bomdestino.maps.ocorrencias.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class OcorrenciaDTO {

    @NotNull(message = "Latitude deve ser informada")
    @ApiModelProperty("Coordenada de Latitude")
    private BigDecimal lat;

    @NotNull(message = "Longitude deve ser informada")
    @ApiModelProperty("Coordenada de Longitude")
    private BigDecimal lng;

    @DecimalMin(value = "1", message = "Ao menos uma ocorrencia deve ser notificada")
    @ApiModelProperty("Numero de ocorrencias a serem computadas")
    private Long numeroOcorrencias;

    @Pattern(regexp = "\\S{1,12}", message = "O nome da etiqueta deve ter entre 1 e 12 caracteres sem espaços")
    @NotNull( message = "O nome da etiqueta deve ter entre 1 e 12 caracteres sem espaços")
    @ApiModelProperty("Constante que indica o nome da etiqueta no mapa ex.: GRIPE_A")
    private String etiqueta;
}
