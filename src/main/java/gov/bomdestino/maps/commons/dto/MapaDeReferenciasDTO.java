package gov.bomdestino.maps.commons.dto;

import gov.bomdestino.maps.ocorrencias.data.OcorrenciaDTO;
import gov.bomdestino.maps.pontogeografico.data.PontoGeograficoDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Getter
public class MapaDeReferenciasDTO {
    private List<PontoGeograficoDTO> pontosGeograficos;
    private List<OcorrenciaDTO> ocorrencias;

    public MapaDeReferenciasDTO(){
        ocorrencias = new ArrayList<>();
        pontosGeograficos = new ArrayList<>();
    }

    public void addPonto(PontoGeograficoDTO ponto){
        Objects.requireNonNull(ponto, "Precisa fornecer um ponto válido");
        pontosGeograficos.add(ponto);
    }

    public void addOcorrencia(OcorrenciaDTO ocorrenciaDTO){
        Objects.requireNonNull(ocorrenciaDTO, "Precisa fornecer uma ocorrencia válida");
        ocorrencias.add(ocorrenciaDTO);
    }
}
