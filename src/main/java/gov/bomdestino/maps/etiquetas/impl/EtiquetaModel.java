package gov.bomdestino.maps.etiquetas.impl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name="etiqueta")
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
class EtiquetaModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String titulo;
    private String nome;
}
