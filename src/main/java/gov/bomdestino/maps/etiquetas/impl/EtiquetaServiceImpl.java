package gov.bomdestino.maps.etiquetas.impl;

import gov.bomdestino.maps.etiquetas.EtiquetaService;
import gov.bomdestino.maps.etiquetas.data.EtiquetaDTO;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
class EtiquetaServiceImpl implements EtiquetaService {
    private final EtiquetaRepository etiquetaRepository;
    private final ModelMapper modelMapper;

    @Override
    public List<EtiquetaDTO> buscaTodas() {
        return etiquetaRepository.findAll()
                                 .stream()
                                 .map(e -> new EtiquetaDTO(e.getNome(), e.getTitulo()))
                                 .collect(Collectors.toList());
    }

    @Override
    public EtiquetaDTO buscaPeloNome(String nome) {
        return etiquetaRepository.findByNome(nome)
                .map(e -> new EtiquetaDTO(e.getNome(), e.getTitulo()))
                .orElseThrow(() ->
                        new EntityNotFoundException("Não foi possível encontrar uma etiqueta com o nome: "+nome));
    }

    @Override
    public void validaEtiqueta(String nome) {
        buscaPeloNome(nome);
    }

    @Override
    public void salvar(EtiquetaDTO dto) {
        etiquetaRepository.save(new EtiquetaModel(0l, dto.getTitulo(), dto.getNome()));
    }
}
