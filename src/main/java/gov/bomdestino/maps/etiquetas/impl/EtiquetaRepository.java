package gov.bomdestino.maps.etiquetas.impl;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

interface EtiquetaRepository extends JpaRepository<EtiquetaModel, Long> {
    Optional<EtiquetaModel> findByNome(String nome);
}
