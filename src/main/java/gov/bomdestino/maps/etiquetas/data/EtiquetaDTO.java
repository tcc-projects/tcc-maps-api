package gov.bomdestino.maps.etiquetas.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class EtiquetaDTO {

    @Pattern(regexp = "\\S{1,12}", message = "O nome da etiqueta deve ter entre 1 e 12 caracteres sem espaços")
    @NotNull( message = "O nome da etiqueta deve ter entre 1 e 12 caracteres sem espaços")
    @ApiModelProperty("Constante que indica o nome da etiqueta no mapa ex.: GRIPE_A")
    private String nome;

    @NotBlank(message = "O Título da etiqueta é obrigatório")
    @ApiModelProperty("Titulo que descreve o que aponta a etiqueta")
    private String titulo;
}
