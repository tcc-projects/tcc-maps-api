package gov.bomdestino.maps.etiquetas;

import gov.bomdestino.maps.etiquetas.data.EtiquetaDTO;

import java.util.List;

public interface EtiquetaService {
    List<EtiquetaDTO> buscaTodas();
    EtiquetaDTO buscaPeloNome(String nome);
    void validaEtiqueta(String nome);
    void salvar(EtiquetaDTO dto);

}
