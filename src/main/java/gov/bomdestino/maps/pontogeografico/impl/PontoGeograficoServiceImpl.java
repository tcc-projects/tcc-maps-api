package gov.bomdestino.maps.pontogeografico.impl;

import gov.bomdestino.maps.etiquetas.EtiquetaService;
import gov.bomdestino.maps.pontogeografico.PontoGeograficoService;
import gov.bomdestino.maps.pontogeografico.data.PontoGeograficoDTO;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
class PontoGeograficoServiceImpl implements PontoGeograficoService {
    private final ModelMapper mapper;
    private final PontoGeograficoRepository pontoRepository;
    private final EtiquetaService etiquetaService;

    @Override
    public Long salvar(PontoGeograficoDTO ponto) {
       etiquetaService.validaEtiqueta(ponto.getEtiqueta());
       return pontoRepository.save(mapper.map(ponto, PontoGeograficoModel.class)).getId();
    }

    @Override
    public List<PontoGeograficoDTO> buscarTodos() {
        return pontoRepository.findAll()
                              .stream()
                              .map(p-> mapper.map(p, PontoGeograficoDTO.class))
                              .collect(Collectors.toList());
    }
}
