package gov.bomdestino.maps.pontogeografico.impl;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name="pontogeografico")
@Getter
@Setter
class PontoGeograficoModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(precision = 19, scale = 6, columnDefinition="DECIMAL(19,6)")
    private BigDecimal lat;
    @Column(precision = 19, scale = 6, columnDefinition="DECIMAL(19,6)")
    private BigDecimal lng;
    private String etiqueta;
    private String titulo;
    private String descricao;
}
