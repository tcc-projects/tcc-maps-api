package gov.bomdestino.maps.pontogeografico.impl;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface PontoGeograficoRepository extends JpaRepository<PontoGeograficoModel, Long> {
}
