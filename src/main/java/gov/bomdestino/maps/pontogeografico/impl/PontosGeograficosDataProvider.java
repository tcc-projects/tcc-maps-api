package gov.bomdestino.maps.pontogeografico.impl;

import gov.bomdestino.maps.pontogeografico.data.PontoGeograficoDTO;

import java.math.BigDecimal;
import java.util.List;

public class PontosGeograficosDataProvider {

    static List<PontoGeograficoModel> pontosGeograficos(){
        var p1 = new PontoGeograficoModel();

        p1.setId(1L);
        p1.setEtiqueta("A");
        p1.setLat(BigDecimal.ONE);
        p1.setLng(BigDecimal.ONE);
        p1.setDescricao("descricao 1");
        p1.setTitulo("titulo 1");

        var p2 = new PontoGeograficoModel();
        p2.setId(2L);
        p2.setEtiqueta("B");
        p2.setLat(BigDecimal.TEN);
        p2.setLng(BigDecimal.TEN);
        p2.setDescricao("descricao 2");
        p2.setTitulo("titulo 2");

        return List.of(p1, p2);
    }

    public static List<PontoGeograficoDTO> pontosGeograficosDTO(){
        var p1 = new PontoGeograficoDTO();
        p1.setEtiqueta("A");
        p1.setLat(BigDecimal.ONE);
        p1.setLng(BigDecimal.ONE);
        p1.setDescricao("descricao 1");
        p1.setTitulo("titulo 1");

        var p2 = new PontoGeograficoDTO();

        p2.setEtiqueta("B");
        p2.setLat(BigDecimal.TEN);
        p2.setLng(BigDecimal.TEN);
        p2.setDescricao("descricao 2");
        p2.setTitulo("titulo 2");

        return List.of(p1, p2);
    }
}
