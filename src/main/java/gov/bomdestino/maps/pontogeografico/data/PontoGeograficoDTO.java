package gov.bomdestino.maps.pontogeografico.data;


import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class PontoGeograficoDTO {

    @NotNull(message = "Latitude deve ser informada")
    @ApiModelProperty("Coordenada de Latitude")
    private BigDecimal lat;

    @NotNull(message = "Longitude deve ser informada")
    @ApiModelProperty("Coordenada de Longitude")
    private BigDecimal lng;

    @Pattern(regexp = "\\S{1,12}", message = "O nome da etiqueta deve ter entre 1 e 12 caracteres sem espaços")
    @NotNull( message = "O nome da etiqueta deve ter entre 1 e 12 caracteres sem espaços")
    @ApiModelProperty("Constante que indica o nome da etiqueta no mapa ex.: GRIPE_A")
    private String etiqueta;

    @NotBlank(message = "Titulo deve ter entre 3 e 20 caracteres")
    @Size(min = 3, max = 20, message = "Titulo deve ter entre 3 e 20 caracteres")
    private String titulo;

    private String descricao;
}
