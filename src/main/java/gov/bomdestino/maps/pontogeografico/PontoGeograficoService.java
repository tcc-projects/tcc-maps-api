package gov.bomdestino.maps.pontogeografico;

import gov.bomdestino.maps.pontogeografico.data.PontoGeograficoDTO;

import java.util.List;

public interface PontoGeograficoService {
   Long salvar(PontoGeograficoDTO ponto);
   List<PontoGeograficoDTO> buscarTodos();
}
