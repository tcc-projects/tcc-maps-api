package gov.bomdestino.maps.etiquetas.data;

import gov.bomdestino.maps.util.ValidationTest;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EtiquetaDTOTest extends ValidationTest {

    @ParameterizedTest(name ="{index} - O nome ''{0}'', não é valido")
    @MethodSource("nomesInvalidos")
    void deveriaApontarErrosDeValidacaoNoNome(String nome){
        var etiqueta = new EtiquetaDTO(nome, "titulo valido");
        var validationError = getViolationMessage("nome", etiqueta);
        assertEquals("O nome da etiqueta deve ter entre 1 e 12 caracteres sem espaços", validationError);
    }


    @ParameterizedTest(name ="{index} - O titulo ''{0}'', não é valido")
    @MethodSource("titulosInvalidos")
    void deveriaApontarErrosDeValidacaoNoTitulo(String titulo){
        var etiqueta = new EtiquetaDTO("NOME_VALIDO", titulo);
        var validationError = getViolationMessage("titulo", etiqueta);
        assertEquals("O Título da etiqueta é obrigatório", validationError);
    }


    static Stream<String> nomesInvalidos(){
        return Stream.of(" ", "", null, "AAAAAAAAAAAAA", "AA AA");
    }
    static Stream<String> titulosInvalidos(){
        return Stream.of(" ", "", null);
    }

}