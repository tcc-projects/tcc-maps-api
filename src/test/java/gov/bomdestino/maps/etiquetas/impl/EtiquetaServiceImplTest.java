package gov.bomdestino.maps.etiquetas.impl;

import gov.bomdestino.maps.etiquetas.data.EtiquetaDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

import static gov.bomdestino.maps.etiquetas.impl.EtiquetasDataProvider.etiquetas;
import static gov.bomdestino.maps.etiquetas.impl.EtiquetasDataProvider.etiquetasDTO;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EtiquetaServiceImplTest {

    static ModelMapper modelMapper = new ModelMapper();

    @Mock
    EtiquetaRepository etiquetaRepository;

    @Captor
    ArgumentCaptor<EtiquetaModel> etiquetaCaptor;

    EtiquetaServiceImpl etiquetaService;

    @BeforeEach
    void init(){
        etiquetaService = new EtiquetaServiceImpl(etiquetaRepository, modelMapper);
    }

    @Test
    void deveriaBuscarEtiquetasETransformarEmDTO(){
        when(etiquetaRepository.findAll()).thenReturn(etiquetas());

        List<EtiquetaDTO> etiquetasEncontradas = etiquetaService.buscaTodas();
        assertEquals(etiquetasDTO(), etiquetasEncontradas);
        verify(etiquetaRepository).findAll();
    }

    @Test
    void deveriaEncontrarUmEtiquetaPeloNome(){
        EtiquetaModel etiqueta = new EtiquetaModel();
        etiqueta.setNome("A");
        when(etiquetaRepository.findByNome("A")).thenReturn(Optional.of(etiqueta));
        EtiquetaDTO etiquetaEncontrada = etiquetaService.buscaPeloNome("A");

        assertEquals("A", etiquetaEncontrada.getNome());
        verify(etiquetaRepository).findByNome("A");
    }

    @Test
    void deveriaLancarExceptionSeEtiquetaNaoEncontrada(){
        when(etiquetaRepository.findByNome("A")).thenReturn(Optional.empty());
        assertThrows(EntityNotFoundException.class, () -> etiquetaService.buscaPeloNome("A"));
        verify(etiquetaRepository).findByNome("A");
    }

    @Test
    void deveriaGravarUmaEtiquetaComDadosDoDTO(){
        var novaEtiqueta = new EtiquetaDTO("Nova", "Etiqueta");
        etiquetaService.salvar(novaEtiqueta);
        verify(etiquetaRepository).save(etiquetaCaptor.capture());
        var etiquetaSalva = etiquetaCaptor.getValue();
        assertEquals(novaEtiqueta.getNome(), etiquetaSalva.getNome());
        assertEquals(novaEtiqueta.getTitulo(), etiquetaSalva.getTitulo());
    }
}