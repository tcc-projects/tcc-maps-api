package gov.bomdestino.maps.etiquetas.impl;

import gov.bomdestino.maps.etiquetas.data.EtiquetaDTO;

import java.util.List;

public class EtiquetasDataProvider {

    static List<EtiquetaModel> etiquetas(){
        EtiquetaModel e = new EtiquetaModel();
        e.setId(1l);
        e.setNome("A");
        e.setTitulo("Titulo A");


        EtiquetaModel e2 = new EtiquetaModel();
        e2.setId(2l);
        e2.setNome("B");
        e2.setTitulo("Titulo B");

        return List.of(e,e2);
    }

    public static List<EtiquetaDTO> etiquetasDTO(){
        return List.of(new EtiquetaDTO("A", "Titulo A"), new EtiquetaDTO("B", "Titulo B"));
    }
}
