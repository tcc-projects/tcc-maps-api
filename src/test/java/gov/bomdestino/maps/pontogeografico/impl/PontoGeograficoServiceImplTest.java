package gov.bomdestino.maps.pontogeografico.impl;

import gov.bomdestino.maps.etiquetas.EtiquetaService;

import gov.bomdestino.maps.pontogeografico.data.PontoGeograficoDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.util.List;

import static gov.bomdestino.maps.pontogeografico.impl.PontosGeograficosDataProvider.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PontoGeograficoServiceImplTest {

    static ModelMapper modelMapper = new ModelMapper();

    @Mock
    PontoGeograficoRepository pontoGeograficoRepository;
    @Mock
    EtiquetaService etiquetaService;
    @Captor
    ArgumentCaptor<PontoGeograficoModel> pontoGeograficoCaptor;

    PontoGeograficoServiceImpl pontoGeograficoService;

    @BeforeEach
    void init(){
        pontoGeograficoService = new PontoGeograficoServiceImpl(modelMapper, pontoGeograficoRepository, etiquetaService);
    }

    @Test
    void deveriaBuscarPontoGeograficosETransformarEmDTO(){
        when(pontoGeograficoRepository.findAll()).thenReturn(pontosGeograficos());
        List<PontoGeograficoDTO> pontoGeograficosEncontradas = pontoGeograficoService.buscarTodos();
        assertEquals(pontosGeograficosDTO(), pontoGeograficosEncontradas);
        verify(pontoGeograficoRepository).findAll();
    }

    @Test
    void naoDeveriaSalvarPontoGeograficoComEtiquetaInvalida(){
        PontoGeograficoDTO pontoGeografico = new PontoGeograficoDTO();
        pontoGeografico.setEtiqueta("A");
        doThrow(new EntityNotFoundException()).when(etiquetaService).validaEtiqueta("A");
        assertThrows(EntityNotFoundException.class, ()->pontoGeograficoService.salvar(pontoGeografico));
        verifyNoInteractions(pontoGeograficoRepository);
    }

    @Test
    void deveriaSalvarUmNovoPonto(){
        var pontoGeografico = new PontoGeograficoDTO();
        pontoGeografico.setEtiqueta("A");
        pontoGeografico.setLat(BigDecimal.ONE);
        pontoGeografico.setLng(BigDecimal.ONE);
        pontoGeografico.setDescricao("descricao 1");
        pontoGeografico.setTitulo("titulo 1");

        var pontoGeograficoModel = new PontoGeograficoModel();
        pontoGeograficoModel.setId(1L);

        when(pontoGeograficoRepository.save(any())).thenReturn(pontoGeograficoModel);

        pontoGeograficoService.salvar(pontoGeografico);
        verify(pontoGeograficoRepository).save(pontoGeograficoCaptor.capture());
        PontoGeograficoModel pontoGravado = pontoGeograficoCaptor.getValue();

        assertNull(pontoGravado.getId());
        assertEquals(pontoGeografico.getEtiqueta(), pontoGravado.getEtiqueta());
        assertEquals(pontoGeografico.getLat(), pontoGravado.getLat());
        assertEquals(pontoGeografico.getLng(), pontoGravado.getLng());
        assertEquals(pontoGeografico.getDescricao(), pontoGravado.getDescricao());
        assertEquals(pontoGeografico.getTitulo(), pontoGravado.getTitulo());
    }

}
