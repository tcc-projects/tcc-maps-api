package gov.bomdestino.maps.pontogeografico.data;

import gov.bomdestino.maps.util.ValidationTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static gov.bomdestino.maps.util.StringRandomUtil.randomWord;
import static org.junit.jupiter.api.Assertions.*;

class PontoGeograficoDTOTest extends ValidationTest {
    private PontoGeograficoDTO pontoGeografico;

    @BeforeEach
    void init(){
        pontoGeografico = new PontoGeograficoDTO();
    }

    @ParameterizedTest(name ="{index} - A etiqueta ''{0}'', não é valida")
    @MethodSource("etiquetasInvalidas")
    void deveriaApontarErrosDeValidacaoNaEtiqueta(String etiqueta){
        pontoGeografico.setEtiqueta(etiqueta);
        var validationError = getViolationMessage("etiqueta", pontoGeografico);
        assertEquals("O nome da etiqueta deve ter entre 1 e 12 caracteres sem espaços", validationError);
    }

    @ParameterizedTest(name ="{index} - O Titulo ''{0}'', não é valido")
    @MethodSource("titulosInvalidos")
    void deveriaApontarErrosDeValidacaoNoTitulo(String titulo){
        pontoGeografico.setEtiqueta(titulo);
        var validationError = getViolationMessage("titulo", pontoGeografico);
        assertEquals("Titulo deve ter entre 3 e 20 caracteres", validationError);
    }

    @Test
    void deveHaverCoordenadaDeLongitude(){
        var validationError = getViolationMessage("lng", pontoGeografico);
        assertEquals("Longitude deve ser informada", validationError);
    }

    @Test
    void deveHaverCoordenadaDeLatitude(){
        var validationError = getViolationMessage("lat", pontoGeografico);
        assertEquals("Latitude deve ser informada", validationError);
    }

    static Stream<String> etiquetasInvalidas(){
        return Stream.of(" ", "", null, "AAAAAAAAAAAAA", "AA AA");
    }
    static Stream<String> titulosInvalidos(){
        return Stream.of(" ", "", null, randomWord(21), "AA");
    }

}