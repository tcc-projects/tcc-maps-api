package gov.bomdestino.maps.referencias.impl;

import gov.bomdestino.maps.commons.dto.MapaDeReferenciasDTO;
import gov.bomdestino.maps.ocorrencias.OcorrenciaService;
import gov.bomdestino.maps.ocorrencias.data.OcorrenciaDTO;
import gov.bomdestino.maps.pontogeografico.PontoGeograficoService;
import gov.bomdestino.maps.pontogeografico.data.PontoGeograficoDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MapaFacadeImplTest {

    @Mock
    PontoGeograficoService pontoService;
    @Mock
    OcorrenciaService ocorrenciaService;

    @InjectMocks
    MapaFacadeImpl mapaFacade;

    @Test
    void deveriaBuscarTodosOsPontosEOcorrencias(){
        List<PontoGeograficoDTO> pontos = List.of(new PontoGeograficoDTO());
        List<OcorrenciaDTO> ocorrencias = List.of(new OcorrenciaDTO());

        when(pontoService.buscarTodos()).thenReturn(pontos);
        when(ocorrenciaService.buscaTodas()).thenReturn(ocorrencias);
        MapaDeReferenciasDTO referenciasEncontradas = mapaFacade.buscaReferencias();

        assertEquals(pontos, referenciasEncontradas.getPontosGeograficos());
        assertEquals(ocorrencias, referenciasEncontradas.getOcorrencias());

        verify(pontoService).buscarTodos();
        verify(ocorrenciaService).buscaTodas();
    }

}
