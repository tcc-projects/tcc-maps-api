package gov.bomdestino.maps.api;

import gov.bomdestino.maps.commons.dto.MapaDeReferenciasDTO;
import gov.bomdestino.maps.ocorrencias.data.OcorrenciaDTO;
import gov.bomdestino.maps.pontogeografico.data.PontoGeograficoDTO;
import gov.bomdestino.maps.referencias.MapaFacade;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static gov.bomdestino.maps.etiquetas.impl.EtiquetasDataProvider.etiquetasDTO;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = MapaController.class)
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
class MapaControllerTest {

    @Autowired
    MockMvc http;

    @MockBean
    MapaFacade mapaFacade;

    @Test
    void deveriaRetornarOkComListaDePontosEOcorrencias() throws Exception {
        MapaDeReferenciasDTO referencias = new MapaDeReferenciasDTO();
        referencias.addOcorrencia(new OcorrenciaDTO());
        referencias.addPonto(new PontoGeograficoDTO());
        when(mapaFacade.buscaReferencias()).thenReturn(referencias);

        http.perform(get("/mapa/referencias")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.pontosGeograficos").value(hasSize(1)))
                .andExpect(jsonPath("$.ocorrencias").value(hasSize(1)));

        verify(mapaFacade).buscaReferencias();
    }
}