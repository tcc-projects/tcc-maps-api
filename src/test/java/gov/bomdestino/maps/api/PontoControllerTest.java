package gov.bomdestino.maps.api;

import gov.bomdestino.maps.pontogeografico.PontoGeograficoService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.greaterThan;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = PontoController.class)
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
class PontoControllerTest {
    static String pontoValido = "{\"etiqueta\":\"A\",\"lat\":-32.8572983,\"lng\":-52.8273238,\"titulo\":\"Titulo legal\",\"descricao\":\"descricao maneira\"}";
    static String pontoinValido = "{\"etiqueta\":\"A A\",\"lat\":-32.8572983,\"lng\":-52.8273238,\"titulo\":\"Titulo legal\",\"descricao\":\"descricao maneira\"}";

    @Autowired
    MockMvc http;

    @MockBean
    PontoGeograficoService pontoService;


    @Test
    void deveriaCriarUmNovoPontoGeografico() throws Exception {
        http.perform(post("/mapa/pontos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pontoValido))
                .andDo(print())
                .andExpect(status().isCreated());
        verify(pontoService).salvar(any());
    }

    @Test
    void deveriaRetornarBadRequestComListaDeErrosSeJsonInvalido() throws Exception {
        http.perform(post("/mapa/pontos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pontoinValido))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.erros.length()").value(greaterThan(0)));

        verifyNoInteractions(pontoService);
    }
}