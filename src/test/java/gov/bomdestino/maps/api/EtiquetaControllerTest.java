package gov.bomdestino.maps.api;

import gov.bomdestino.maps.etiquetas.EtiquetaService;
import gov.bomdestino.maps.etiquetas.impl.EtiquetasDataProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static gov.bomdestino.maps.etiquetas.impl.EtiquetasDataProvider.*;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(value = EtiquetaController.class)
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
class EtiquetaControllerTest {

    static String etiquetaValida = "{\"nome\": \"A\", \"titulo\": \"Atendimentos\"}";
    static String etiquetaInvalida = "{\"nome\": \"A B\", \"titulo\": \"Atendimentos\"}";

    @Autowired
    MockMvc http;

    @MockBean
    EtiquetaService etiquetaService;

    @Test
    void deveriaRetornarOkComUmaListaDeEtiquetas() throws Exception {
        when(etiquetaService.buscaTodas()).thenReturn(etiquetasDTO());
        http.perform(get("/mapa/etiquetas")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(hasSize(2)));
        verify(etiquetaService).buscaTodas();
    }

    @Test
    void deveriaRetornarOkMesmoSeListaDeEtiquetasVazia() throws Exception {
        when(etiquetaService.buscaTodas()).thenReturn(new ArrayList<>());
        http.perform(get("/mapa/etiquetas")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(hasSize(0)));
        verify(etiquetaService).buscaTodas();
    }

    @Test
    void deveriaCriarUmaNovaEtiqueta() throws Exception {
        http.perform(post("/mapa/etiquetas")
                .contentType(MediaType.APPLICATION_JSON)
                .content(etiquetaValida))
                .andExpect(status().isCreated());
        verify(etiquetaService).salvar(any());
    }

    @Test
    void deveriaRetornarBadRequestComListaDeErrosSeJsonInvalido() throws Exception {
        http.perform(post("/mapa/etiquetas")
                .contentType(MediaType.APPLICATION_JSON)
                .content(etiquetaInvalida))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.erros.length()").value(greaterThan(0)));

        verifyNoInteractions(etiquetaService);
    }
}