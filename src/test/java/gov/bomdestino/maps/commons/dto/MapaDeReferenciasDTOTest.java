package gov.bomdestino.maps.commons.dto;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MapaDeReferenciasDTOTest {

    MapaDeReferenciasDTO referencias = new MapaDeReferenciasDTO();

    @Test
    void naoDeveAceitarPontoNulo(){
        NullPointerException ex = assertThrows(NullPointerException.class, () -> referencias.addPonto(null));
        assertEquals("Precisa fornecer um ponto válido", ex.getMessage());
    }

    @Test
    void naoDeveAceitarOcorrenciaNula(){
        NullPointerException ex = assertThrows(NullPointerException.class, () -> referencias.addOcorrencia(null));
        assertEquals("Precisa fornecer uma ocorrencia válida", ex.getMessage());
    }
}