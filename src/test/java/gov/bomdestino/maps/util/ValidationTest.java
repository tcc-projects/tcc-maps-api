package gov.bomdestino.maps.util;

import gov.bomdestino.maps.etiquetas.data.EtiquetaDTO;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

public class ValidationTest {
    static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public String getViolationMessage(String field, Object obj){
        return validator.validate(obj)
                    .stream()
                    .filter(violation -> violation.getPropertyPath()
                                                .toString()
                                                .equals(field))
                    .findFirst()
                    .map(v -> v.getMessage())
                .orElseThrow(()-> new ViolationNotFoundException("Nenhuma validação foi violada no campo '"+field+"'"));
    }
}
