package gov.bomdestino.maps.util;

public class ViolationNotFoundException extends RuntimeException {
    public ViolationNotFoundException(String msg) {
        super(msg);
    }
}
