package gov.bomdestino.maps.ocorrencias.impl;

import gov.bomdestino.maps.etiquetas.EtiquetaService;
import gov.bomdestino.maps.ocorrencias.data.OcorrenciaDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static gov.bomdestino.maps.ocorrencias.impl.OcorrenciaDataProvider.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OcorrenciaServiceImplTest {
    static ModelMapper modelMapper = new ModelMapper();

    @Mock
    OcorrenciaRepository ocorrenciaRepository;
    @Mock
    EtiquetaService etiquetaService;
    @Captor
    ArgumentCaptor<OcorrenciaModel> ocorrenciaCaptor;

    OcorrenciaServiceImpl ocorrenciaService;

    @BeforeEach
    void init(){
        ocorrenciaService = new OcorrenciaServiceImpl(modelMapper, ocorrenciaRepository, etiquetaService);
    }

    @Test
    void deveriaBuscarOcorrenciasETransformarEmDTO(){
        when(ocorrenciaRepository.findAll()).thenReturn(ocorrencias());
        List<OcorrenciaDTO> ocorrenciasEncontradas = ocorrenciaService.buscaTodas();
        assertEquals(ocorrenciasDTO(), ocorrenciasEncontradas);
        verify(ocorrenciaRepository).findAll();
    }

    @Test
    void naoDeveriaSalvarOcorrenciaComEtiquetaInvalida(){
        OcorrenciaDTO ocorrencia = new OcorrenciaDTO();
        ocorrencia.setEtiqueta("A");
        doThrow(new EntityNotFoundException()).when(etiquetaService).validaEtiqueta("A");
        assertThrows(EntityNotFoundException.class, ()->ocorrenciaService.salvarOcorrencia(ocorrencia));
        verifyNoInteractions(ocorrenciaRepository);
    }

    @Test
    void deveriaGravarUmaNovaOcorrencia(){
        OcorrenciaDTO ocorrenciaDTO = new OcorrenciaDTO();
        ocorrenciaDTO.setLat(BigDecimal.ONE);
        ocorrenciaDTO.setLng(BigDecimal.ONE);
        ocorrenciaDTO.setEtiqueta("A");
        ocorrenciaDTO.setNumeroOcorrencias(10L);

        when(ocorrenciaRepository.findByLatAndLngAndEtiqueta(BigDecimal.ONE, BigDecimal.ONE, "A"))
                .thenReturn(Optional.empty());

        ocorrenciaService.salvarOcorrencia(ocorrenciaDTO);

        verify(etiquetaService).validaEtiqueta("A");
        verify(ocorrenciaRepository).findByLatAndLngAndEtiqueta(BigDecimal.ONE, BigDecimal.ONE, "A");
        verify(ocorrenciaRepository).save(ocorrenciaCaptor.capture());
        OcorrenciaModel ocorrenciaGravada = ocorrenciaCaptor.getValue();

        assertEquals(ocorrenciaDTO.getLat(), ocorrenciaGravada.getLat());
        assertEquals(ocorrenciaDTO.getLng(), ocorrenciaGravada.getLng());
        assertEquals(ocorrenciaDTO.getEtiqueta(), ocorrenciaGravada.getEtiqueta());
        assertEquals(ocorrenciaDTO.getNumeroOcorrencias(), ocorrenciaGravada.getNumeroOcorrencias());
        assertNull(ocorrenciaGravada.getId());
        assertEquals(LocalDate.now(), ocorrenciaGravada.getDataOcorrencia());
    }

    @Test
    void deveriaAtualizarUmaOcorrenciaSeLocalizacaoEEtiquetaIguais(){
        OcorrenciaDTO ocorrenciaDTO = new OcorrenciaDTO();
        ocorrenciaDTO.setLat(BigDecimal.ONE);
        ocorrenciaDTO.setLng(BigDecimal.ONE);
        ocorrenciaDTO.setEtiqueta("A");
        ocorrenciaDTO.setNumeroOcorrencias(10L);

        OcorrenciaModel model = new OcorrenciaModel();
        model.setId(1L);
        model.setLat(BigDecimal.ONE);
        model.setLng(BigDecimal.ONE);
        model.setEtiqueta("A");
        model.setNumeroOcorrencias(10L);

        when(ocorrenciaRepository.findByLatAndLngAndEtiqueta(BigDecimal.ONE, BigDecimal.ONE, "A"))
                .thenReturn(Optional.of(model));

        ocorrenciaService.salvarOcorrencia(ocorrenciaDTO);

        verify(etiquetaService).validaEtiqueta("A");
        verify(ocorrenciaRepository).findByLatAndLngAndEtiqueta(BigDecimal.ONE, BigDecimal.ONE, "A");
        verify(ocorrenciaRepository).save(ocorrenciaCaptor.capture());
        OcorrenciaModel ocorrenciaAtualizada = ocorrenciaCaptor.getValue();

        assertEquals(ocorrenciaDTO.getLat(), ocorrenciaAtualizada.getLat());
        assertEquals(ocorrenciaDTO.getLng(), ocorrenciaAtualizada.getLng());
        assertEquals(ocorrenciaDTO.getEtiqueta(), ocorrenciaAtualizada.getEtiqueta());
        assertEquals(20, ocorrenciaAtualizada.getNumeroOcorrencias());
        assertEquals(1L, ocorrenciaAtualizada.getId());
        assertEquals(LocalDate.now(), ocorrenciaAtualizada.getDataOcorrencia());

    }
}