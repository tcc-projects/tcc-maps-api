package gov.bomdestino.maps.ocorrencias.impl;

import gov.bomdestino.maps.ocorrencias.data.OcorrenciaDTO;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class OcorrenciaDataProvider {

    static List<OcorrenciaModel> ocorrencias(){
        var o1 = new OcorrenciaModel();
        o1.setId(1L);
        o1.setDataOcorrencia(LocalDate.now());
        o1.setNumeroOcorrencias(10L);
        o1.setEtiqueta("A");
        o1.setLat(BigDecimal.ONE);
        o1.setLng(BigDecimal.ONE);

        var o2 = new OcorrenciaModel();
        o2.setId(2L);
        o2.setDataOcorrencia(LocalDate.now());
        o2.setNumeroOcorrencias(10L);
        o2.setEtiqueta("B");
        o2.setLat(BigDecimal.TEN);
        o2.setLng(BigDecimal.TEN);

        return List.of(o1, o2);
    }


    public static List<OcorrenciaDTO> ocorrenciasDTO(){
        var o1 = new OcorrenciaDTO();
        o1.setNumeroOcorrencias(10L);
        o1.setEtiqueta("A");
        o1.setLat(BigDecimal.ONE);
        o1.setLng(BigDecimal.ONE);

        var o2 = new OcorrenciaDTO();
        o2.setNumeroOcorrencias(10L);
        o2.setEtiqueta("B");
        o2.setLat(BigDecimal.TEN);
        o2.setLng(BigDecimal.TEN);

        return List.of(o1, o2);
    }

    static OcorrenciaModel ocorrenciaSimplesValida(){
        var o1 = new OcorrenciaModel();
        o1.setId(1L);
        o1.setDataOcorrencia(LocalDate.now());
        o1.setNumeroOcorrencias(10L);
        o1.setEtiqueta("A");
        o1.setLat(BigDecimal.ONE);
        o1.setLng(BigDecimal.ONE);
        return o1;
    }
}
