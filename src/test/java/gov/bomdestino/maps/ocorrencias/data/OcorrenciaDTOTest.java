package gov.bomdestino.maps.ocorrencias.data;

import gov.bomdestino.maps.util.ValidationTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OcorrenciaDTOTest extends ValidationTest {
    private OcorrenciaDTO ocorrencia;

    @BeforeEach
    void init(){
        ocorrencia = new OcorrenciaDTO();
    }

    @ParameterizedTest(name ="{index} - A etiqueta ''{0}'', não é valida")
    @MethodSource("etiquetasInvalidas")
    void deveriaApontarErrosDeValidacaoNaEtiqueta(String etiqueta){
        ocorrencia.setEtiqueta(etiqueta);
        var validationError = getViolationMessage("etiqueta", ocorrencia);
        assertEquals("O nome da etiqueta deve ter entre 1 e 12 caracteres sem espaços", validationError);
    }

    @Test
    void deveHaverPeloMenosUmaOcorrencia(){
        ocorrencia.setNumeroOcorrencias(0L);
        var validationError = getViolationMessage("numeroOcorrencias", ocorrencia);
        assertEquals("Ao menos uma ocorrencia deve ser notificada", validationError);
    }

    @Test
    void deveHaverCoordenadaDeLongitude(){
        var validationError = getViolationMessage("lng", ocorrencia);
        assertEquals("Longitude deve ser informada", validationError);
    }

    @Test
    void deveHaverCoordenadaDeLatitude(){
        var validationError = getViolationMessage("lat", ocorrencia);
        assertEquals("Latitude deve ser informada", validationError);
    }

    static Stream<String> etiquetasInvalidas(){
        return Stream.of(" ", "", null, "AAAAAAAAAAAAA", "AA AA");
    }
}