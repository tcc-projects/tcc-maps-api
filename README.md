## Descrição:
Este software prove uma interface REST para criação e recuperação de marcações em mapas da prefeitura.  
Visando integração com sistemas internos ou ainda de parceiros da prefeitura de Bom Destino afim de enriquecer informações que são fornecidas aos munícipes.


## Tecnologias
Neste projeto foi codificado em **Java** utilizando set de frameworks **Spring** :
* Boot
* Data
* MVC
* Security

## Banco de dados
MySQL
